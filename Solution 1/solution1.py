# Solutions by D. Junussov
# 16 Dec 2022

# We need to complete following 6 tasks:
# 1. Find the windiest month - (month and average wind speed)
# 2. Find the coldest month - (month and average temperature)
# 3. Find the coldest day - (day and average temperature)
# 4. Find the warmest month - (month and average temperature)
# 5. Find the warmest day - (day and average temperature)
# 6. Find the rainiest week - (period and amount of precipitation)

# Clarifications
# T - Температура воздуха (градусы Цельсия) на высоте 2 метра над поверхностью земли
# U - Относительная влажность (%) на высоте 2 метра над поверхностью земли
# Ff - Cкорость ветра на высоте 10-12 метров над земной поверхностью, осредненная за 10-минутный период,
# непосредственно предшествовавший сроку наблюдения (метры в секунду)

# Phase 1 - на первой фазе нужно изьять данные с файла и прочистить не нужные данные
import csv
from datetime import datetime

date_time = []
wind_month_dict = dict()
temperature_month_dict = dict()
temperature_day_dict = dict()
rain_week_dict = dict()

with open('file.csv', 'r') as csvfile:
    count = 0
    spamreader = csv.reader(csvfile, delimiter=';')
    for row in spamreader:
        if len(row)!=30:
            continue
        date = datetime.strptime(row[0], '%d.%m.%Y %H:%M')
        date_time.append(date)
        if wind_month_dict.get((date.month, date.year)) is None:
            wind_month_dict[(date.month, date.year)] = [float(row[7])]
        else:
            wind_month_dict[(date.month, date.year)].append(float(row[7]))

        if temperature_month_dict.get((date.month, date.year)) is None:
            temperature_month_dict[(date.month, date.year)] = [float(row[1])]
        else:
            temperature_month_dict[(date.month, date.year)].append(float(row[1]))

        if temperature_day_dict.get((date.day, date.month, date.year)) is None:
            temperature_day_dict[(date.day, date.month, date.year)] = [float(row[1])]
        else:
            temperature_day_dict[(date.day, date.month, date.year)].append(float(row[1]))

        # в переданном файле отсутсвует одно значение для U
        if row[5] == '':
            humidity = 0
        else:
            humidity = int(row[5])
        if rain_week_dict.get(date.isocalendar()[1]) is None:
            rain_week_dict[date.isocalendar()[1]] = [humidity]
        else:
            rain_week_dict[date.isocalendar()[1]].append(humidity)

# 1. Find the windiest month - (month and average wind speed)
windiest_month = tuple()
windiest_value = None

for key, value in wind_month_dict.items():
    average = sum(value) / len(value)
    if windiest_value is None or average > windiest_value:
        windiest_value = average
        windiest_month = key

print(f"Windiest months is {windiest_month}: {windiest_value}")

# 2. Find the coldest month - (month and average temperature)
coldest_month = tuple()
coldest_value = None

for key, value in temperature_month_dict.items():
    average = sum(value) / len(value)
    if coldest_value is None or average < coldest_value:
        coldest_value = average
        coldest_month = key

print(f"Coldest months is {coldest_month}: {coldest_value}")

# 3. Find the coldest day - (day and average temperature)
coldest_day = tuple()
coldest_value = None

for key, value in temperature_day_dict.items():
    average = sum(value) / len(value)
    if coldest_value is None or average < coldest_value:
        coldest_value = average
        coldest_day = key

print(f"Coldest day is {coldest_day}: {coldest_value}")

# 4. Find the warmest month - (month and average temperature)
warmest_month = tuple()
warmest_value = None

for key, value in temperature_month_dict.items():
    average = sum(value) / len(value)
    if warmest_value is None or average > warmest_value:
        warmest_value = average
        warmest_month = key

print(f"Warmest month is {warmest_month}: {warmest_value}")

# 5. Find the warmest day - (day and average temperature)
warmest_day = tuple()
warmest_value = None

for key, value in temperature_day_dict.items():
    average = sum(value) / len(value)
    if warmest_value is None or average > warmest_value:
        warmest_value = average
        warmest_day = key

print(f"Warmest day is {warmest_day}: {warmest_value}")


# 6. Find the rainiest week - (period and amount of precipitation)
rainiest_week = 0
rainiest_value = None

for key, value in rain_week_dict.items():
    average = sum(value) / len(value)
    if rainiest_value is None or average > rainiest_value:
        rainiest_value = average
        rainiest_week = key

print(f"Rainiest week is {rainiest_week}: {rainiest_value}")

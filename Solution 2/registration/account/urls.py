from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('', views.dashboard, name='dashboard'),
    path('register/', views.register, name='register'),
    path('edit/', views.edit, name='edit'),
    path('activate/<uidb64>/<token>', views.activate, name='activate'),
    path('ranking/', views.referral_ranking, name='ranking'),
]

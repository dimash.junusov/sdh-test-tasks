from celery import shared_task
from django.core.mail import send_mail
from django.contrib.auth.models import User
from .models import Profile


@shared_task
def user_registered(user):
    """
    Task to send an e-mail notification when an user is
    successfully registered.
    """
    subject = f'Activate your user account. {user.username}'
    message = f'Dear {user.first_name},\n\n' \
              f'You have successfully registered.'
    mail_sent = send_mail(subject,
                          message,
                          'testyurta@gmail.com',
                          [user.email])
    return mail_sent
